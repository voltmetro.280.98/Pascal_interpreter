import sys
from datetime import datetime

import click

from d_pascal_parser import *


def error(msg):
    delim = " "
    sys.stderr.write(str(datetime.now()) + delim + msg + '\n')


def log(msg):
    delim = " "
    sys.stdout.write(str(datetime.now()) + delim + msg + '\n')


def start_pas():
    greeting = "Pascal interpreter\n"
    ind_lvl = 0
    prompt = "{level} "
    env = {}

    print(greeting)

    while True:

        src = input(prompt.format(level='=' * ind_lvl * 2))
        if src.split(' ')[0] == 'while':
            ind_lvl += 1
        if ind_lvl > 0 and src.strip() == 'end':
            ind_lvl -= 1

        # DEBUG
        print(src, type(src))
        tokens = pascal_lexeme(src)
        print(tokens)
        parse_result = pascal_parse(tokens)
        if not parse_result:
            error('Ошибка парсинга!')
            return 1
        ast = parse_result.value
        ast.eval(env)
        log('Значения переменных:')
        for var, val in env.items():
            log('{0}: {1}'.format(str(var), str(val)))


def open_pas(src):
    tokens = pascal_lexeme(src)
    parse_result = pascal_parse(tokens)
    if not parse_result:
        error('Ошибка парсинга!')
        return 1

    ast = parse_result.value
    env = {}
    ast.eval(env)

    log('Переменные после выполнения программы имеют следующие значения:')
    for var, val in env.items():
        log('{0}: {1}'.format(str(var), str(val)))
    return 0


@click.command()
@click.option('-e', '--exec', default='', type=str, help='Execute file')
def main(exec):
    if exec:
        filename = exec
        with open(filename, 'r') as f:
            src = f.read()
        return open_pas(src)
    else:
        return start_pas()


if __name__ == '__main__':
    sys.exit(main())
