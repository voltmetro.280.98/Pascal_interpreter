import d_pascal_input

RESERVED = 'RESERVED'
INT      = 'INT'
ID       = 'ID'

token_exprs = [
    (r'[ \n\t]+', None),
    (r'#[^\n]*',  None),
    (r'//[^\n]*', None),
    (r'\:=',      RESERVED),
    (r'\(',       RESERVED),
    (r'\)',       RESERVED),
    (r';',        RESERVED),
    (r'[\+\-]?[0-9]+([\.,][0-9]+)?',   INT),
    (r'\+',       RESERVED),
    (r'\-',       RESERVED),
    (r'\*',       RESERVED),
    (r'/',        RESERVED),
    (r'<=',       RESERVED),
    (r'<',        RESERVED),
    (r'>=',       RESERVED),
    (r'>',        RESERVED),
    (r'=',        RESERVED),
    (r'~',        RESERVED),
    (r'and',      RESERVED),
    (r'or',       RESERVED),
    (r'not',      RESERVED),
    (r'if',       RESERVED),
    (r'then',     RESERVED),
    (r'else',     RESERVED),
    (r'while',    RESERVED),
    (r'do',       RESERVED),
    (r'repeat',   RESERVED),
    (r'until',    RESERVED),
    (r'end',      RESERVED),
    (r'power',    RESERVED),
    (r'div',      RESERVED),
    (r'mod',      RESERVED),
    (r'power',    RESERVED),
    (r'shr',      RESERVED),
    (r'shl',      RESERVED),
    (r'[A-Za-z][A-Za-z0-9_]*', ID)
]


def pascal_lexeme(source):
    return d_pascal_input.lex(source, token_exprs)