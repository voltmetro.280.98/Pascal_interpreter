import os
import sys
import subprocess
from GUI import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class MyWin(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.Copy_BTN.clicked.connect(self.copyFunction)
        self.ui.Run_BTN.clicked.connect(self.runFunction)
        self.ui.Exit_BTN.clicked.connect(self.pasteFunction)

    def copyFunction(self):
        cbt = self.ui.Code_TE.toPlainText()
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setText(cbt, mode=cb.Clipboard)



    def runFunction(self):
        temp = self.ui.Code_TE.toPlainText() or ""
        with open('executed_file.pa2', 'wt') as f:
            f.write(temp)
        f.close()
        cmd = [os.path.join(os.getcwd(), 'd_pascal.py')]
        cmd += ['-e', 'executed_file.pa2']
        p = subprocess.Popen(["python"] + cmd, stdout=subprocess.PIPE)
        # stdout = subprocess.PIPE - означает, что вывод программы перехватывается
        p.wait()
        Result = p.communicate()[0]  # получаем перехваченный вывод
        Result = str(Result, "UTF-8")
        self.ui.Console_TE.setText(Result)

    def pasteFunction(self):
        def getClipboardData():
            clipboard = QApplication.clipboard()
            return QClipboard.text(clipboard)

        clipboardData = str(getClipboardData())
        self.ui.Code_TE.setText(clipboardData)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    myapp = MyWin()
    myapp.show()
    sys.exit(app.exec_())
