from d_pascal_errors import *
from d_pascal_lexer import *


def keyword(kw):
    return Reserved(kw, RESERVED)

num = Tag(INT) ^ (lambda i: int(i))
id = Tag(ID)


def pascal_parse(tokens):
    ast = parser()(tokens, 0)
    return ast


def parser():
    return Phrase(statements())


def statements():
    separator = keyword(';') ^ (lambda x: lambda l, r: CompoundStatement(l, r))
    return Exp(statement(), separator)


def statement():
    return assign_statement() | \
           if_operator() | \
           while_operator() | \
           repeat_operator()


def assign_statement():
    def process(parsed):
        ((name, _), exp) = parsed
        return AssignStatement(name, exp)
    return id + keyword(':=') + arifmetical() ^ process


def if_operator():
    def process(parsed):
        (((((_, condition), _), true_stmt), false_parsed), _) = parsed
        if false_parsed:
            (_, false_stmt) = false_parsed
        else:
            false_stmt = None
        return IfStatement(condition, true_stmt, false_stmt)
    return keyword('if') + boolean_statement() + \
           keyword('then') + Lazy(statements) + \
           Opt(keyword('else') + Lazy(statements)) + \
           keyword('end') ^ process


def while_operator():
    def process(parsed):
        ((((_, condition), _), body), _) = parsed
        return WhileStatement(condition, body)
    return keyword('while') + boolean_statement() + \
           keyword('do') + Lazy(statements) + \
           keyword('end') ^ process

def repeat_operator():
    def process(parsed):
        ((((_, body), _), condition), _) = parsed
        return DoWhileStatement(body, condition)
    return keyword('repeat') + Lazy(statements) + \
           keyword('until') + boolean_statement() + \
           keyword('end') ^ process



def boolean_statement():
    return precedence(boolean_operators(),
                      boolean_priority,
                      process_logic)


def boolean_operators():
    return boolean_not() | \
           boolean_relations() | \
           boolean_brackets()


def boolean_not():
    return keyword('not') + Lazy(boolean_operators) ^ (lambda parsed: Boolean_Not(parsed[1]))


def boolean_relations():
    relops = ['~', '<', '<=', '>', '>=', '=']
    return arifmetical() + universal_parse(relops) + arifmetical() ^ process_relations


def boolean_brackets():
    return keyword('(') + Lazy(boolean_statement) + keyword(')') ^ process_brackets


def arifmetical():
    return precedence(arifmetical_operators(),
                      arifmetical_priority,
                      process_binary)


def arifmetical_operators():
    return arifmetical_value() | arifmetical_brackets()


def arifmetical_brackets():
    return keyword('(') + Lazy(arifmetical) + keyword(')') ^ process_brackets


def arifmetical_value():
    return (num ^ (lambda i: IntArifmetical(i))) | \
           (id ^ (lambda v: VarArifmetical(v)))


def precedence(value_parser, precedence_levels, combine):
    def op_parser(precedence_level):
        return universal_parse(precedence_level) ^ combine
    parser = value_parser * op_parser(precedence_levels[0])
    for precedence_level in precedence_levels[1:]:
        parser = parser * op_parser(precedence_level)
    return parser


def process_binary(op):
    return lambda l, r: Binary_Arifmetical(op, l, r)


def process_unary(op):
   return lambda r: Unary_Arifmetical(op, r)

arifmetical_priority = [
    ['power'],
    ['*', '/', 'mod', 'div', 'shl', 'shr'],
    ['+', '-'],
]

boolean_priority = [
    ['and'],
    ['or'],
]

def process_relations(parsed):
    ((left, op), right) = parsed
    return Boolean_Relations(op, left, right)


def process_logic(op):
    if op == 'and':
        return lambda l, r: Boolean_And(l, r)
    elif op == 'or':
        return lambda l, r: Boolean_Or(l, r)
    else:
        raise RuntimeError('Unknown operator: ' + op)


def process_brackets(parsed):
    ((_, p), _) = parsed
    return p


def universal_parse(ops):
    op_parsers = [keyword(op) for op in ops]
    from functools import reduce
    parser = reduce(lambda l, r: l | r, op_parsers)
    return parser


