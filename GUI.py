# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Svetlana\Downloads\GUI.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1096, 574)
        Form.setMaximumSize(QtCore.QSize(1096, 574))
        Form.setStyleSheet("color: rgb(255, 255, 255);\n"
"border-top-color: rgb(255, 255, 255);\n"
"border-color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
"border-bottom-color: rgb(255, 255, 255);\n"
"border-right-color: rgb(255, 255, 255);")
        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setGeometry(QtCore.QRect(179, 9, 911, 261))
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayoutWidget = QtWidgets.QWidget(self.groupBox)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(10, 20, 891, 231))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.Code_TE = QtWidgets.QTextEdit(self.horizontalLayoutWidget)
        self.Code_TE.setObjectName("Code_TE")
        self.horizontalLayout.addWidget(self.Code_TE)
        self.groupBox_2 = QtWidgets.QGroupBox(Form)
        self.groupBox_2.setGeometry(QtCore.QRect(179, 309, 911, 261))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayoutWidget_2 = QtWidgets.QWidget(self.groupBox_2)
        self.horizontalLayoutWidget_2.setGeometry(QtCore.QRect(10, 20, 891, 231))
        self.horizontalLayoutWidget_2.setObjectName("horizontalLayoutWidget_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.Console_TE = QtWidgets.QTextEdit(self.horizontalLayoutWidget_2)
        self.Console_TE.setObjectName("Console_TE")
        self.horizontalLayout_2.addWidget(self.Console_TE)
        self.verticalLayoutWidget = QtWidgets.QWidget(Form)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(9, 0, 161, 571))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.Copy_BTN = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.Copy_BTN.setStyleSheet("border-color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 0, 0);\n"
"text-color: rgb(255,255,255);")
        self.Copy_BTN.setObjectName("Copy_BTN")
        self.verticalLayout.addWidget(self.Copy_BTN)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.Run_BTN = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.Run_BTN.setStyleSheet("border-color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 0, 0);\n"
"text-color: rgb(255,255,255);")
        self.Run_BTN.setObjectName("Run_BTN")
        self.verticalLayout.addWidget(self.Run_BTN)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.Exit_BTN = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.Exit_BTN.setStyleSheet("border-color: rgb(255, 255, 255);\n"
"background-color: rgb(255, 0, 0);\n"
"text-color: rgb(255,255,255);")
        self.Exit_BTN.setObjectName("Exit_BTN")
        self.verticalLayout.addWidget(self.Exit_BTN)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.label = QtWidgets.QLabel(Form)
        self.label.setGeometry(QtCore.QRect(190, 270, 901, 41))
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Interpreter by Oleg Bhambri"))
        self.groupBox.setTitle(_translate("Form", "Code"))
        self.groupBox_2.setTitle(_translate("Form", "Console"))
        self.Copy_BTN.setText(_translate("Form", "Copy"))
        self.Run_BTN.setText(_translate("Form", "Run"))
        self.Exit_BTN.setText(_translate("Form", "Paste"))
        self.label.setText(_translate("Form", "<html><head/><body><p align=\"center\">Interpreter by Oleg Bhambri</p></body></html>"))

